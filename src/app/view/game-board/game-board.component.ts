import { Component, OnInit } from '@angular/core';
import { GameServiceService } from '../../service/game-service.service';
import { GAME_EVENT, CELL_STATUS } from '../../type/gameType';

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit {

  public CELL_STATUS = CELL_STATUS;
  public isShowPopup = false;
  public timeIntervalInput;

  constructor( public gameService: GameServiceService ) { }

  ngOnInit(): void {
    this.gameService.gameEvents$.subscribe((e: GAME_EVENT) => {
      console.log(e);
      if (e === GAME_EVENT.GAME_FINISHED) {
        this.isShowPopup = true;
      }
    });
  }

  startNewGame(): void {
    const options = {
      timeIntervalMS: this.timeIntervalInput * 1,
      xSize: 10,
      ySize: 10,
      winScore: 10
    };

    this.closePopup();
    this.gameService.createNewGame(options);
  }

  closePopup(): void {
    this.isShowPopup = false;
  }

}
