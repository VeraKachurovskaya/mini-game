import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { GameServiceService } from '../../service/game-service.service';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  constructor( public gameService: GameServiceService ) { }

  ngOnInit(): void {
  }

}
