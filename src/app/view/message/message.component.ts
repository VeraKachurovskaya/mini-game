import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GameServiceService } from '../../service/game-service.service';
import { CELL_STATUS } from '../../type/gameType';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  public CELL_STATUS = CELL_STATUS;

  @Output() closeMessage = new EventEmitter<void>();

  constructor(
    public gameService: GameServiceService,
  ) { }

  ngOnInit(): void {
  }
}
