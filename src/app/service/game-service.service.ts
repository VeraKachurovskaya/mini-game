import { Injectable } from '@angular/core';
import { CELL_STATUS, GAME_EVENT, IGameScore } from '../type/gameType';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameServiceService {

  public gameMap: CELL_STATUS [] [];
  public drawingCell: { x: number, y: number };
  public gameScore: IGameScore;
  private timeIntervalMS: number;
  private winScore: number;

  public gameEvents$ = new Subject<GAME_EVENT>();

  private roundTimeoutID;

  constructor() {
    this.gameMap = this.createNewGameMap(10, 10);
    this.gameScore = {
      winner: null,
      userCount: 0,
      computerCount: 0,
    };
  }

  private createNewGameMap(x: number, y: number): CELL_STATUS [] [] {
    const arrayCells = [];

    for (let i = 0; i < x; i++) {
      arrayCells.push(new Array(y).fill(CELL_STATUS.FREE));
    }

    return arrayCells;
  }

  private calcXY(num: number, colSize: number): { x: number, y: number } {
    const x = Math.trunc(num / colSize);
    const y = num - (colSize * x);

    return {x, y};
  }

  private getDrawCell(num: number, gameMap: CELL_STATUS [] []): { x: number, y: number } {
    let result = {
      x: 0,
      y: 0
    };

    while (gameMap[result.x][result.y] !== CELL_STATUS.FREE) {
      result = this.calcXY(num - 1, gameMap[0].length);
      num++;
      if (num >= (gameMap.length * gameMap[0].length)) {
        num = 0;
      }
    }

    return result;
  }

  private calcNewRound(gameMap: CELL_STATUS [] []): void {
    const cellNum = gameMap.length * gameMap[0].length;
    const randomNum = Math.floor(Math.random() * cellNum) + 1;

    this.drawingCell = this.getDrawCell(randomNum, gameMap);

  }

  private updateScore(player: CELL_STATUS): void {
    if (player === CELL_STATUS.USER) {
      this.gameScore.userCount++;
    }
    if (player === CELL_STATUS.COMPUTER) {
      this.gameScore.computerCount++;
    }
  }

  private checkWinner(gameScore: IGameScore, winScore: number): CELL_STATUS.USER | CELL_STATUS.COMPUTER {
    if (gameScore.userCount >= winScore) {
      return CELL_STATUS.USER;
    }

    if (gameScore.computerCount >= winScore) {
      return CELL_STATUS.COMPUTER;
    }

    return null;
  }

  private startNewRound(): void {
    this.calcNewRound(this.gameMap);

    this.gameEvents$.next(GAME_EVENT.NEW_ROUND);

    this.roundTimeoutID = setTimeout(() => {
      this.handlePlayerAction(this.drawingCell.x, this.drawingCell.y, CELL_STATUS.COMPUTER);
    }, this.timeIntervalMS);
  }

  public handlePlayerAction(x: number, y: number, player: CELL_STATUS): void {
    if (x !== this.drawingCell.x || y !== this.drawingCell.y) {
      return;
    }

    clearTimeout(this.roundTimeoutID);

    this.gameMap[x][y] = player;

    this.updateScore(player);

    this.gameScore.winner = this.checkWinner(this.gameScore, this.winScore);

    if (this.gameScore.winner) {
      this.gameEvents$.next(GAME_EVENT.GAME_FINISHED);
      return;
    }

    this.startNewRound();
  }

  public createNewGame(options: { timeIntervalMS: number, xSize: number, ySize: number, winScore: number }): void {
    this.gameMap = this.createNewGameMap(options.xSize, options.ySize);
    this.timeIntervalMS = options.timeIntervalMS;
    this.winScore = options.winScore;

    this.drawingCell = null;
    this.gameScore = {
      winner: null,
      userCount: 0,
      computerCount: 0,
    };

    this.startNewRound();
  }
}


