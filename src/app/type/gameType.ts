export enum CELL_STATUS {
    COMPUTER = 'COMPUTER',
    USER = 'USER',
    FREE = 'FREE',
  }

export enum GAME_EVENT {
    NEW_ROUND = 'NEW_ROUND',
    GAME_FINISHED = 'GAME_FINISHED',
}

export interface IGameScore {
    userCount: number;
    computerCount: number;
    winner: CELL_STATUS.USER | CELL_STATUS.COMPUTER;
}
